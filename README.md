# Catastrophe

## Getting started

- Requires yarn
- Requires at least node 14

## Installing

```bash
$ yarn
```

## Development mode

```bash
$ yarn dev
```

## Production mode

```bash
$ yarn start
```

## Unit tests

Unit tests are setup - but I've not added any tests...

```bash
$ yarn test
```

or

```bash
$ yarn test:coverage
```

## Notes

- To "log out" from the fake "login" process, delete the "api_key" cookie, there's no validation included

const express = require("express");
const { createProxyMiddleware } = require("http-proxy-middleware");

const {
  CATS_API_URL = "https://api.thecatapi.com/v1/",
  PORT = 3001,
} = process.env;

// Would usually use a logger library like winston instead of console
const { log } = console;

const server = () => {
  const app = express();

  app.use(express.static("dist"));

  app.use(
    "/api",
    createProxyMiddleware({
      target: CATS_API_URL,
      changeOrigin: true,
      secure: false,
      pathRewrite: {
        "^/api/cats": "/",
      },
    })
  );

  app.listen(PORT, () => log(`Catastrophe app listening on port ${PORT}`));
};

module.exports = server();

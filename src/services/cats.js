import { del, get, multipart, post } from "../fetch";

export const addCat = (file) => multipart("api/cats/images/upload", file);

export const addFavourite = (body) => post("/api/cats/favourites", body);

export const getCats = () => get("/api/cats/images?limit=100&order=DESC");

export const getFavourites = () => get("/api/cats/favourites");

export const getVotes = () => get("/api/cats/votes");

export const removeFavourite = (id) => del(`/api/cats/favourites/${id}`);

export const updateVote = (body) => post("/api/cats/votes", body);

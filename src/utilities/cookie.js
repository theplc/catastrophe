import Cookie from "js-cookie";

export const set = (key, value, options) => Cookie.set(key, value, options);

export const get = (key) => Cookie.get(key);

export const remove = (key) => Cookie.remove(key);

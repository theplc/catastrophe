import { combineReducers } from "@reduxjs/toolkit";
import cats from "./cats";

export default combineReducers({
  cats,
});

import {
  createAsyncThunk,
  createEntityAdapter,
  createSlice,
} from "@reduxjs/toolkit";
import { find, filter, get, map, sum } from "lodash";
import * as catsService from "../services/cats";

export const catsAdapter = createEntityAdapter();

export const addCat = createAsyncThunk("cats/addCat", async (files) =>
  catsService.addCat(files[0])
);

export const getCats = createAsyncThunk("cats/getCats", async () =>
  Promise.all([
    await catsService.getCats(),
    await catsService.getFavourites(),
    await catsService.getVotes(),
  ])
);

export const getCatsFulfilled = (state, { payload }) => {
  const [cats, favourites, votes] = payload;
  const catsWithAdditionalProperties = map(cats, (cat) => {
    const { id } = cat;
    const votesList = map(
      filter(votes, (vote) => vote.image_id === id),
      ({ value }) => (value === 0 ? -1 : value)
    );

    return {
      ...cat,
      favouriteId: get(find(favourites, { image_id: id }), "id", null),
      votes: sum(votesList),
    };
  });

  catsAdapter.upsertMany(state, catsWithAdditionalProperties);
};

export const addFavourite = createAsyncThunk(
  "cats/addFavourite",
  async (body) => catsService.addFavourite(body)
);

export const removeFavourite = createAsyncThunk(
  "cats/removeFavourite",
  async (id) => catsService.removeFavourite(id)
);

export const updateVote = createAsyncThunk("cats/updateVote", async (body) =>
  catsService.updateVote(body)
);

const initialState = catsAdapter.getInitialState();

const catsSlice = createSlice({
  name: "cats",
  initialState,
  reducers: {
    upsertCat: catsAdapter.upsertOne,
  },
  extraReducers: (builder) => {
    builder.addCase(getCats.fulfilled, getCatsFulfilled);
  },
});

const { actions, reducer } = catsSlice;
const { upsertCat } = actions;

export const catsActions = {
  addCat,
  addFavourite,
  getCats,
  removeFavourite,
  updateVote,
  upsertCat,
};

export const catsSelectors = catsAdapter.getSelectors(({ cats }) => cats);

export const {
  selectById: selectCatById,
  selectIds: selectCatIds,
  selectEntities: selectCatEntities,
  selectAll: selectAllCats,
  selectTotal: selectTotalCats,
} = catsSelectors;

export default reducer;

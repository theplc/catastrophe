import React from "react";
import { render } from "react-dom";
import { Provider } from "react-redux";
import createStore from "./store";
import Routes from "./routes";
import "./styles";

const { store } = createStore();

render(
  <Provider store={store}>
    <Routes />
  </Provider>,
  document.getElementById("root")
);

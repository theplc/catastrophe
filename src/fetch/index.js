import axios from "axios";
import { merge } from "lodash";
import * as cookie from "../utilities/cookie";

const fetch = async (url, options) => {
  const defaults = {
    method: "GET",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "x-api-key": cookie.get("api_key"),
    },
  };

  try {
    const { data } = await axios({
      url,
      ...merge(defaults, options),
    });

    return data;
  } catch (error) {
    throw Error(error);
  }
};

export const del = async (url, data, options) =>
  fetch(url, {
    ...options,
    method: "DELETE",
    data,
  });

export const multipart = async (url, file, options) => {
  const defaults = {
    method: "POST",
    headers: {
      "Content-Type": "multipart/form-data",
    },
  };

  const data = new FormData();

  data.append("file", file);

  return fetch(url, {
    ...merge(defaults, options),
    data,
  });
};

export const post = async (url, data, options) =>
  fetch(url, {
    ...options,
    method: "POST",
    data,
  });

export const get = async (url, options) => fetch(url, options);

export default fetch;

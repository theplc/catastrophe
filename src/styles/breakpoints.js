export const xs = "480px";
export const sm = "640px";
export const md = "768px";
export const lg = "768px";
export const xl = "1280px";

import React from "react";
import { HashRouter, NavLink, Route, Switch } from "react-router-dom";
import PrivateRoute from "./private";
import { HomePage, LoginPage, UploadPage } from "./pages";

const Routes = () => {
  return (
    <HashRouter>
      <nav
        id="main-nav"
        className="navbar"
        role="navigation"
        aria-label="main navigation"
      >
        <ul className="navbar-start">
          <li className="navbar-item">
            <NavLink to="/" activeClassName="is-active" exact>
              View your cats
            </NavLink>
          </li>
          <li className="navbar-item">
            <NavLink to="/upload" activeClassName="is-active" exact>
              Upload a cat
            </NavLink>
          </li>
        </ul>
      </nav>

      <main id="main" className="container">
        <Switch>
          <PrivateRoute exact path="/">
            <HomePage />
          </PrivateRoute>
          <PrivateRoute exact path="/upload">
            <UploadPage />
          </PrivateRoute>
          <Route exact path="/login" component={LoginPage} />
        </Switch>
      </main>
    </HashRouter>
  );
};

export default Routes;

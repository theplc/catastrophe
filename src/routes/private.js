import React from "react";
import PropTypes from "prop-types";
import { Route } from "react-router-dom";
import { isUndefined } from "lodash";
import * as cookie from "../utilities/cookie";
import { LoginPage } from "./pages";

export const Private = ({ children, ...rest }) => (
  <Route
    {...rest}
    render={() => {
      const hasCookie = !isUndefined(cookie.get("api_key"));

      if (hasCookie) {
        return children;
      }

      return <LoginPage />;
    }}
  />
);

Private.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Private;

import HomePage from "./home";
import LoginPage from "./login";
import UploadPage from "./upload";

export { HomePage, LoginPage, UploadPage };

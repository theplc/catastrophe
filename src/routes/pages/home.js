import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { map } from "lodash";
import { catsActions, selectAllCats } from "../../reducers/cats";
import { Favourite, Vote } from "../../components";
import {
  StyledCatCard,
  StyledCatDetails,
  StyledCatList,
  StyledHero,
  StyledImageLink,
  StyledProgressWrapper,
} from "./styles";

const mapStateToProps = (state) => ({
  cats: selectAllCats(state),
});

export const Home = ({
  addFavourite,
  cats,
  getCats,
  removeFavourite,
  updateVote,
}) => {
  const [loaded, setLoaded] = useState(false);

  useEffect(() => {
    const loadData = async () => {
      await getCats();
      setLoaded(true);
    };

    loadData();
  }, [getCats]);

  return (
    <>
      {loaded === false && (
        <StyledProgressWrapper>
          <progress className="progress is-info" max="100">
            70%
          </progress>
        </StyledProgressWrapper>
      )}

      {cats.length === 0 && loaded && (
        <StyledHero className="hero is-primary is-medium">
          <div className="hero-body">
            <div className="container">
              <h1>
                Looks like you haven&apos;t <Link to="/upload">uploaded</Link>{" "}
                any cats yet.
              </h1>
            </div>
          </div>
        </StyledHero>
      )}
      <StyledCatList>
        {cats.length > 0 &&
          map(cats, ({ favouriteId, id, url, votes }) => (
            <StyledCatCard key={id}>
              <StyledImageLink href={url} target="_blank" rel="noreferrer">
                <img src={url} alt="" />
              </StyledImageLink>

              <StyledCatDetails>
                <Favourite
                  favouriteId={favouriteId}
                  id={id}
                  add={addFavourite}
                  remove={removeFavourite}
                  sync={getCats}
                />

                <Vote id={id} update={updateVote} votes={votes} />
              </StyledCatDetails>
            </StyledCatCard>
          ))}
      </StyledCatList>
    </>
  );
};

Home.defaultProps = {
  cats: [],
};

Home.propTypes = {
  addFavourite: PropTypes.func.isRequired,
  cats: PropTypes.arrayOf(
    PropTypes.shape({
      breeds: PropTypes.arrayOf(PropTypes.string),
      id: PropTypes.string,
      url: PropTypes.string,
      width: PropTypes.number,
      height: PropTypes.number,
      sub_id: PropTypes.string,
      created_at: PropTypes.string,
      original_filename: PropTypes.string,
    })
  ),
  getCats: PropTypes.func.isRequired,
  removeFavourite: PropTypes.func.isRequired,
  updateVote: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, {
  ...catsActions,
})(Home);

import React, { useState } from "react";
import PropTypes from "prop-types";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { get } from "lodash";
import { catsActions } from "../../reducers/cats";
import { DropZone } from "../../components";
import { StyledUploadProgressWrapper } from "./styles";

const Upload = ({ addCat }) => {
  const [errorMessage, setErrorMessage] = useState();
  const [redirect, setRedirect] = useState(false);
  const [isActive, setIsActive] = useState(false);

  const handleDrop = async (file) => {
    setIsActive(true);

    const response = await addCat(file);

    setIsActive(false);

    if (response.error) {
      setErrorMessage(get(response, "error.message"));
    } else {
      setRedirect(true);
    }
  };

  if (redirect === true) {
    return <Redirect to="/" />;
  }

  return (
    <div className="section">
      {errorMessage && (
        <div className="notification is-warning">
          <p>{errorMessage}</p>
        </div>
      )}
      <DropZone onDrop={handleDrop} />
      {isActive === true && (
        <StyledUploadProgressWrapper>
          <progress className="progress is-info" max="100">
            70%
          </progress>
        </StyledUploadProgressWrapper>
      )}
    </div>
  );
};

Upload.propTypes = {
  addCat: PropTypes.func.isRequired,
};

export default connect(null, {
  ...catsActions,
})(Upload);

import styled from "@emotion/styled";
import { md, xs } from "../../styles/breakpoints";

export const StyledHero = styled.div`
  margin-top: 20px;
`;

export const StyledCatList = styled.ul`
  display: flex;
  flex: 1 1 auto;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: flex-start;
  padding: 20px 10px;
  width: 100%;
`;

export const StyledCatCard = styled.li`
  background: white;
  margin-bottom: 20px;
  overflow: hidden;

  @media (min-width: ${xs}) {
    margin-left: 1%;
    margin-right: 1%;
    width: 48%;
  }

  @media (min-width: ${md}) {
    width: 23%;
  }
`;

export const StyledImageLink = styled.a`
  background: #999;
  display: block;
  height: 160px;
  overflow: hidden;

  @media (min-width: ${xs}) {
    height: 200px;
  }

  @media (min-width: ${md}) {
    height: 160px;
  }

  img {
    object-fit: cover;
    min-width: 100%;
    transition: transform 0.2s ease-in-out;

    &:hover,
    &:active,
    &:focus {
      transform: scale(1.3);
    }
  }
`;

export const StyledCatDetails = styled.div`
  padding: 10px;
`;

export const StyledLoginFormContainer = styled.div`
  background: #fff;
  margin-top: 40px;
  padding: 20px;
`;

export const StyledLoginForm = styled.form`
  padding: 40px 20px;
  width: 100%;
`;

export const StyledProgressWrapper = styled.div`
  position: absolute;
  top: 0;
  left: 10px;
  right: 10px;
`;

export const StyledUploadProgressWrapper = styled.div`
  margin: 0 20px;
`;

import React, { useState } from "react";
import { Redirect } from "react-router-dom";
import * as cookie from "../../utilities/cookie";
import { StyledLoginForm, StyledLoginFormContainer } from "./styles";

const Login = () => {
  const [apiKey, setAPiKey] = useState("");
  const [redirect, setRedirect] = useState(false);

  const handleChange = (event) => {
    const { target } = event;
    const { value } = target;

    setAPiKey(value);
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    cookie.set("api_key", apiKey, { expires: 1 });
    setRedirect(true);
  };

  if (redirect === true || cookie.get("api_key")) {
    return <Redirect to="/" />;
  }

  return (
    <StyledLoginFormContainer className="container">
      <StyledLoginForm onSubmit={handleSubmit}>
        <h1 className="title">Login with your API Key</h1>

        <div className="field mb-5">
          <label className="label" htmlFor="api-Key">
            API Key
          </label>
          <div className="control">
            <input
              className="input"
              id="api-key"
              name="api-key"
              type="text"
              onChange={handleChange}
              value={apiKey}
            />
          </div>
        </div>

        <div className="control">
          <button
            className="button is-primary"
            type="submit"
            onClick={handleSubmit}
            disabled={!apiKey.trim().length}
          >
            Login
          </button>
        </div>
      </StyledLoginForm>
    </StyledLoginFormContainer>
  );
};

export default Login;

import styled from "@emotion/styled";

export const StyledContainer = styled.div`
  padding-bottom: 20px;
  text-align: center;
`;

export const StyledButton = styled.button`
  padding: 10px 30px 10px 10px;
  position: relative;

  &:hover,
  &:active,
  &:focus {
    span {
      opacity: 0.6;
    }
  }
`;

export const StyledIcon = styled.span`
  background-image: url("../../assets/images/filled-heart.png");
  background-image: url("../../assets/images/${({ favourited }) =>
    favourited ? "empty-heart" : "filled-heart"}.png");
  background-position: center center;
  background-repeat: no-repeat;
  background-size: cover;
  display: block;
  height: 14px;
  position: absolute;
  right: 6px;
  top: 13px;
  transition: opacity 0.1s;
  width: 16px;
`;

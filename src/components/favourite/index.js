import React, { useState } from "react";
import PropTypes from "prop-types";
import { StyledButton, StyledContainer, StyledIcon } from "./styles";

const Favourite = ({ add, id, favouriteId, remove, sync }) => {
  const [isActive, setIsActive] = useState();

  const handleClick = async () => {
    let response;

    setIsActive(true);

    if (favouriteId) {
      response = await remove(favouriteId);
    } else {
      response = await add({ image_id: id });
    }

    if (!response.error) {
      await sync();
    }

    setIsActive(false);
  };

  return (
    <StyledContainer>
      <StyledButton
        className={`button is-text  ${isActive ? "is-loading" : ""}`}
        type="button"
        onClick={handleClick}
        disabled={isActive}
      >
        {favouriteId ? "unfavourite" : "favourite"}
        <StyledIcon favourited={!!favouriteId} />
      </StyledButton>
    </StyledContainer>
  );
};

Favourite.defaultProps = {
  favouriteId: null,
};

Favourite.propTypes = {
  add: PropTypes.func.isRequired,
  favouriteId: PropTypes.number,
  id: PropTypes.string.isRequired,
  remove: PropTypes.func.isRequired,
  sync: PropTypes.func.isRequired,
};

export default Favourite;

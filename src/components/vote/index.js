import React, { useState } from "react";
import PropTypes from "prop-types";
import { StyledButtonGroup, StyledVoteText } from "./styles";

const Vote = ({ id, update, votes }) => {
  const [optimisticVotes, setOptimisticVotes] = useState(0);

  // TODO - handle voting errors
  const handleVoteUp = () => {
    setOptimisticVotes(optimisticVotes + 1);

    update({
      image_id: id,
      value: 1,
    });
  };

  // TODO - handle voting errors
  const handleVoteDown = () => {
    setOptimisticVotes(optimisticVotes - 1);

    update({
      image_id: id,
      value: 0,
    });
  };

  return (
    <div>
      <StyledVoteText>Cat score: {votes + optimisticVotes}</StyledVoteText>

      <StyledButtonGroup className="buttons are-small">
        <button
          className="button is-primary"
          type="button"
          onClick={handleVoteUp}
        >
          Upvote
        </button>
        <button
          className="button is-danger"
          type="button"
          onClick={handleVoteDown}
        >
          Downvote
        </button>
      </StyledButtonGroup>
    </div>
  );
};

Vote.defaultProps = {
  votes: 0,
};

Vote.propTypes = {
  id: PropTypes.string.isRequired,
  update: PropTypes.func.isRequired,
  votes: PropTypes.number,
};

export default Vote;

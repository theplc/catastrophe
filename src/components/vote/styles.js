import styled from "@emotion/styled";

export const StyledVoteText = styled.p`
  font-size: 1.4rem;
  padding-bottom: 20px;
  text-align: center;
`;

export const StyledButtonGroup = styled.div`
  display: flex;
  flex: 1 1 auto;
  justify-content: space-between;

  button {
    width: 47%;
  }
`;
/*
// /*  */
// padding-top:
// `; */

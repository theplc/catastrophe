import React from "react";
import PropTypes from "prop-types";
import { useDropzone } from "react-dropzone";
import { StyledDropzone } from "./styles";

const DropZone = ({ onDrop }) => {
  const handleDrop = (file) => {
    onDrop(file);
  };

  const { getRootProps, getInputProps, isDragActive } = useDropzone({
    accept: "image/jpeg, image/png",
    multiple: false,
    onDrop: handleDrop,
  });

  return (
    <StyledDropzone className="hero is-primary is-medium" {...getRootProps()}>
      <div className="hero-body">
        <div className="container">
          <input {...getInputProps()} />
          {isDragActive ? (
            <p>Almost there! Drop the file here...</p>
          ) : (
            <p>Drag and drop or click to select file</p>
          )}
        </div>
      </div>
    </StyledDropzone>
  );
};

DropZone.propTypes = {
  onDrop: PropTypes.func.isRequired,
};

export default DropZone;

import styled from "@emotion/styled";

export const StyledDropzone = styled.div`
  background: #fff;
  cursor: pointer;
  margin: 20px;
  padding: 20px;

  input {
    outline: 0;
    position: relative;
  }

  p {
    text-align: center;
    width: 100%;
  }
`;

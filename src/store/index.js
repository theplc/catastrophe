import { configureStore, getDefaultMiddleware } from "@reduxjs/toolkit";
import { get } from "lodash";
import reducer from "../reducers";

export default (m = module) => {
  const store = configureStore({
    reducer,
    middleware: getDefaultMiddleware(),
    enhancers: [],
  });

  if (process.env.NODE_ENV !== "production" && get(m, "hot")) {
    m.hot.accept("../reducers", () => store.replaceReducer(reducer));
  }

  return {
    store,
  };
};

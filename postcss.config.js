const flexbugsfixes = require("postcss-flexbugs-fixes");
const postcsspresetenv = require("postcss-preset-env");

module.exports = {
  plugins: [flexbugsfixes(), postcsspresetenv({ stage: 0 })],
};

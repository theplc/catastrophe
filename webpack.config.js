const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const ReactRefreshWebpackPlugin = require("@pmmmwh/react-refresh-webpack-plugin");

const dist = path.resolve(__dirname, "dist");
const src = path.resolve(__dirname, "src");
const { NODE_ENV } = process.env;

const isDev = NODE_ENV !== "production";

const styleloader = isDev ? "style-loader" : MiniCssExtractPlugin.loader;

module.exports = {
  mode: NODE_ENV,
  context: src,
  entry: {
    main: ["core-js/stable", "regenerator-runtime/runtime", "./index.js"],
  },
  output: {
    filename: "js/[name].[hash].js",
    path: dist,
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [
          {
            loader: require.resolve("babel-loader"),
            options: {
              // ... other options
              plugins: [
                // ... other plugins
                isDev && require.resolve("react-refresh/babel"),
              ].filter(Boolean),
            },
          },
        ],
      },
      {
        test: /\.css$/,
        use: [styleloader, "css-loader", "postcss-loader"],
        include: [path.join(src, "styles")],
      },
      {
        test: /\.(gif|jpe?g|png)$/,
        use: [
          {
            loader: "url-loader",
            options: {
              limit: 25000,
              name: "images/[name].[ext]",
              publicPath: "../",
            },
          },
        ],
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: "index.html",
    }),
    isDev && new ReactRefreshWebpackPlugin(),
  ].filter(Boolean),
};

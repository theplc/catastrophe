const path = require("path");
const { merge } = require("webpack-merge");
const baseConfig = require("./webpack.config");

const {
  CATS_API_URL = "https://api.thecatapi.com/v1/",
  PORT: port = 3000,
} = process.env;

module.exports = merge(baseConfig, {
  devtool: "eval-cheap-source-map",
  devServer: {
    contentBase: path.resolve(__dirname, "./src"),
    disableHostCheck: true,
    historyApiFallback: true,
    hot: true,
    port,
    proxy: [
      {
        context: ["/api"],
        target: CATS_API_URL,
        changeOrigin: true,
        secure: false,
        pathRewrite: {
          "^/api/cats": "/",
        },
      },
    ],
  },
});
